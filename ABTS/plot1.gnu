set terminal pngcairo enhanced size 400,300 font 'Times New Roman,SimSun'
set output "ABTS.1.提取液.png"
set title off "不同提取液浓度对ABTS的抑制率"
set xlabel "提取液稀释比例"
set ylabel "抑制率(%)"
set key off
unset border
set border 3 lt -1
set xtics nomirror
unset x2tics
set ytics nomirror
unset y2tics
set mxtics 2
set mytics 2
set xrange [0:1.04]
set yrange [0:100]
plot 'data1' notitle with points pt 7 lt -1, \
     'data1' smooth csplines with lines lt -1
