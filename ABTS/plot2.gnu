set terminal pngcairo enhanced size 400,300 font 'Times New Roman,SimSun'
set output "ABTS.2.醇.png"
set title off "不同醇沉上清液浓度对ABTS的抑制率"
set xlabel "醇沉上清液稀释比例"
set ylabel "抑制率(%)"
set key off
unset border
set border 3 lt -1
set xtics nomirror
unset x2tics
set ytics nomirror
unset y2tics
set mxtics 2
set mytics 1
set xrange [0.16:1.04]
plot 'data2' notitle with points pt 7 lt -1, \
     'data2' smooth csplines with lines lt -1
