set terminal pngcairo enhanced size 400,300 font 'Times New Roman,SimSun'
set output "DPPH.3.多糖浓度.png"
set title off "不同多糖浓度对DPPH的抑制率"
set xlabel "多糖浓度(mg/mL)"
set ylabel "抑制率(%)"
set key off
unset border
set border 3 lt -1
set xtics nomirror
unset x2tics
set ytics nomirror
unset y2tics
set mxtics 2
set mytics 1
set xrange [0:10.5]
plot 'data3' notitle with points pt 7 lt -1, \
     'data3' smooth csplines with lines lt -1
