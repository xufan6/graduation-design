set terminal pngcairo enhanced size 600,400 font 'Times New Roman,SimSun'
set output "FRAP标线.png"
set title off "FRAP-Fe^2^+"
set key off
unset border
set border 3 lt -1
set xtics nomirror
unset x2tics
set ytics nomirror
unset y2tics
set xrange [0:1.1]
set mxtics 4
set mytics 2
set xlabel "Fe^2^+(mmol/L)"
set ylabel "OD_{734nm}"
set label 1 "y = 0.5488 x + 0.07314 \n R^2 = 0.9968" at 0.35,0.575
plot 'data' notitle with points pt 7 lt -1, \
    0.5488*x+0.07314 notitle with lines lt -1, \
    'data' with errorbars lt -1
