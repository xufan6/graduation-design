set terminal pngcairo enhanced size 400,300 font 'Times New Roman,SimSun'
set output "1.时间.png"
set title off "不同时间对多糖含量影响"
set xlabel "提取时间(min)"
set ylabel "多糖含量(mg/g)"
set key off
unset border
set border 3 lt -1
set xtics nomirror
unset x2tics
set ytics nomirror
unset y2tics
set mxtics 1
set mytics 1
set xrange [16:54]
plot 'data1' notitle with points pt 7 lt -1, \
     'data1' smooth csplines with lines lt -1
