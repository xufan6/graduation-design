set terminal pngcairo enhanced size 400,300 font 'Times New Roman,SimSun'
set output "2.温度.png"
set title off "不同温度对多糖含量影响"
set xlabel "提取温度(℃)"
set ylabel "多糖含量(mg/g)"
set key off
unset border
set border 3 lt -1
set xtics nomirror
unset x2tics
set ytics nomirror
unset y2tics
set mxtics 1
set mytics 1
set xrange [76:104]
plot 'data2' notitle with points pt 7 lt -1, \
     'data2' smooth csplines with lines lt -1
