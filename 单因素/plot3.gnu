set terminal pngcairo enhanced size 400,300 font 'Times New Roman,SimSun'
set output "3.料液比.png"
set title off "不同料液比对多糖含量影响"
set xlabel "料液比"
set ylabel "多糖含量(mg/g)"
set key off
unset border
set border 3 lt -1
set xtics nomirror
unset x2tics
set ytics nomirror
unset y2tics
set mxtics 1
set mytics 1
set xrange [16:54]
plot 'data3' notitle with points pt 7 lt -1, \
     'data3' smooth csplines with lines lt -1
