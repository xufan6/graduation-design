set terminal pngcairo enhanced size 400,300 font 'Times New Roman,SimSun'
set output "多糖标线.png"
set title off "葡萄糖标准曲线"
set key off
unset border
set border 3 lt -1
set xtics nomirror
unset x2tics
set ytics nomirror
unset y2tics
set xrange [0.01:0.13]
set mxtics 2
set xlabel "葡萄糖浓度(mg/mL)"
set ylabel "OD_{490nm}"
set label 1 "y = 6.055 x + 0.0742 \n R^2 = 0.9964" at 0.03,0.75
plot 'data' notitle with points pt 7 lt -1, \
     6.055*x+0.0742 notitle with lines lt -1, \
     'data' with errorbars lt -1
