set terminal pngcairo enhanced size 600,400 font 'Times New Roman,SimSun'
set output "酚实测.png"
set title off "酚含量"
set key off
unset border
set border 3 lt -1
set xtics nomirror
unset x2tics
set ytics nomirror
unset y2tics
set xrange [1:11]
set mxtics 5
set mytics 2
set xlabel "焦性没食子酸当量(10^-^8mol)"
set ylabel "OD_{765nm}"
set arrow from 7.5,0.322 to 9.306,0.320
set label 1 '提取液' at 7.5,0.322 right
set arrow from 6.8,0.312 to 8.45,0.3123
set label 2 '4mg/mL多糖溶解液' at 6.8,0.312 right
set arrow from 2.8,0.297 to 2.5,0.277
set label 3 '醇沉上清液' at 2.85,0.297 left
plot 'data' notitle with points pt 7 lt -1 lw 1, \
     'data' smooth bezier with lines lt -1, \
     'point' notitle with points pt 3 lt -1 lw 3
