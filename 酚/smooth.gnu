set terminal pngcairo enhanced size 600,400 font 'Times New Roman,SimSun'
set output "酚标线（光滑）.png"
set title off "酚拟合曲线"
set key off
unset border
set border 3 lt -1
set xtics nomirror
unset x2tics
set ytics nomirror
unset y2tics
set xrange [1:11]
set mxtics 5
set mytics 2
set xlabel "焦性没食子酸(10^-^8mol)"
set ylabel "OD_{765nm}"
set label 1 "y = 0.009135 x + 0.234 \n R^2 = 0.9948" at 5.5,0.32
plot 'data' notitle with points pt 7 lt -1, \
     0.009135*x+0.234 notitle with lines lt -1, \
     'data' with errorbars lt -1
